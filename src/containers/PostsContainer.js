import { connect } from 'react-redux';
import Posts from '../components/Posts';

const mapStateToProps = (state) => {
    return {
        posts: state.general.get('posts'),
    };
};

const PostsContainer = connect(
    mapStateToProps,
)(Posts);

export default PostsContainer;

