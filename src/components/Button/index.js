import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Button.css';

const Button = (({ getDataFromJSONPlaceholder }) => {
    return (
        <div className="Button" onClick={getDataFromJSONPlaceholder}>
            Click the button
        </div>
    );
});

Button.propTypes = {
    getDataFromJSONPlaceholder: PropTypes.func.isRequired,
};

export default Button;
